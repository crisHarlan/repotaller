var productosObtenidos;

function getProducts(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 || this.status == 200) {
      //console.table(JSON.parse(request.responseText).value)
      productosObtenidos = JSON.parse(request.responseText).value;
      procesarProductos();
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarProductos() {
  var jsonProductos = productosObtenidos;
  var divTabla = document.getElementById("divTablaProductos");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-hover");
  for (var i = 0; i < jsonProductos.length; i++) {
    //console.log(jsonProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = jsonProductos[i].ProductName;

    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = jsonProductos[i].ProductPrice;

    var columnaStock = document.createElement("td");
    columnaStock.innerText = jsonProductos[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);

  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
